﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
      
        public void FindElement(int[] arr)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            for (int i =0; i< arr.Length; i++)
            {
                if (dict.ContainsKey(arr[i]))
                {
                    dict[arr[i]] = dict[arr[i]] + 1;
                }
                else
                {
                    dict.Add(arr[i], 1);
                }
                
            }
            foreach (KeyValuePair<int, int> item in dict)
            {
                if(item.Value == 1)
                {
                    Console.WriteLine("Key: {0}", item.Key);
                }
               
            }

        }
        static void Main(string[] args)
        {
            Program obj = new Program();
            int[] arr = new int[] { 1, 7, 1, 6, 4, 7, 6 ,5};
            obj.FindElement(arr);
            Console.ReadKey();
        }
    }
}
